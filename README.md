# Lab5 -- Integration testing

## Homework
Solution of lab presented by the link https://colab.research.google.com/drive/150JYvjPTS29NFLcPkQzy8W0YwKNfDM-u?usp=sharing

## BVA Table
|  Parameter         |   Equivalence classes               |
|--------------------|-------------------------------------|
| types              | 'nonsense', 'budget', 'luxury'      |
| plans              | 'fixed_price', 'minute', 'nonsense' |
| distances          | <=0, >0, empty                      |
| planned_distances  | <=0, >0, empty                      |
| times              | <=0, >0, empty                      |
| discounts          | 'yes', 'no', 'nonsense'             |
| planned_time       | <=0, >0, empty                      |

## Decision table
|                    |                                     | R1       | R2       | R3       | R4    | R5    | R6    | R7    | R8  | R9  | R10 | R11 | R12         | R13         | R14         | R15    | R16    | R17    | R18    |
|--------------------|-------------------------------------|----------|----------|----------|-------|-------|-------|-------|-----|-----|-----|-----|-------------|-------------|-------------|--------|--------|--------|--------|
| types              | 'nonsense', 'budget', 'luxury'      | nonsense | *        | *        | *     | *     | *     | *     | *   | *   | *   | *   | luxury      | budget      | budget      | budget | budget | luxury | luxury |
| plans              | 'fixed_price', 'minute', 'nonsense' | *        | nonsense | *        | *     | *     | *     | *     | *   | *   | *   | *   | fixed_price | fixed_price | fixed_price | minute | minute | minute | minute |
| distances          | <=0, >0, empty                      | *        | *        | *        | empty | *     | *     | *     | <=0 | *   | *   | *   | *           | >0          | >0          | >0     | >0     | >0     | >0     |
| planned_distances  | <=0, >0, empty                      | *        | *        | *        | *     | empty | *     | *     | *   | <=0 | *   | *   | >0          | >0          | >0          | >0     | >0     | >0     | >0     |
| times              | <=0, >0, empty                      | *        | *        | *        | *     | *     | empty | *     | *   | *   | <=0 | *   | >0          | >0          | >0          | >0     | >0     | >0     | >0     |
| discounts          | 'yes', 'no', 'nonsense'             | *        | *        | nonsense | *     | *     | *     | *     | *   | *   | *   | *   | *           | yes         | no          | yes    | no     | yes    | no     |
| planned_time       | <=0, >0, empty                      | *        | *        | *        | *     | *     | *     | empty | *   | *   | *   | <=0 | >0          | >0          | >0          | >0     | >0     | >0     | >0     |
| 200                |                                     |          |          |          |       |       |       |       |     |     |     |     |             | X           | X           | X      | X      | X      | X      |
| Invalid Request    |                                     | X        | X        | X        | X     | X     | X     | X     | X   | X   | X   | X   | X           |             |             |        |        |        |        |
| error              |                                     |          |          |          |       |       |       |       |     |     |     |     |             | err         | err         | err    | err    | no err | no err |

if you use the code under the comments, the coverage will be 100%, BUT it is extremely difficult to process

## Bugs

1.   Miscalculations of budget cars prices
2.   Luxury Fixed Price logic does not work
